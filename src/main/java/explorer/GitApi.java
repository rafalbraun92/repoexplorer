package explorer;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.eclipse.jgit.util.io.DisabledOutputStream;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GitApi {

    public static String[] getAllBranches(String repoPath) throws IOException, GitAPIException {
        ArrayList<String> array = new ArrayList<>();

        Repository repo = new FileRepository(repoPath);
        Git git = new Git(repo);

        List<Ref> branches = git.branchList().call();
        for(Ref ref : branches) {
            array.add(ref.getName());
        }
        return Arrays.copyOf(array.toArray(), array.size(), String[].class);
    }

    public static String[] getAllCommitsPerBranch(String repoPath, String branchName) throws IOException, GitAPIException {
        ArrayList<String> array = new ArrayList<>();

        Repository repository = new FileRepository(repoPath);
        Git git = new Git(repository);

        ObjectId branchId = repository.resolve(branchName);
        Iterable<RevCommit> commits = git.log().add(branchId).call();
        int count = 0;
        for (RevCommit commit : commits) {
            array.add(commit.getId().abbreviate( 8 ).name() + " " + commit.getShortMessage());
        }

        return Arrays.copyOf(array.toArray(), array.size(), String[].class);
    }

    public static void checkoutToCommit(String repoPath, String commitHash) throws IOException, GitAPIException {
        ArrayList<String> array = new ArrayList<>();

        Repository repository = new FileRepository(repoPath);
        Git git = new Git(repository);

        git.checkout().setName(commitHash).call();
    }

    public static String[] getAllFilesPerCommit(String repoPath, String commitHashPrev, String commitHashNext) throws IOException, GitAPIException {
        ArrayList<String> array = new ArrayList<>();
        Repository repository = new FileRepository(repoPath);
        Git git = new Git(repository);

        ObjectReader reader = git.getRepository().newObjectReader();
        CanonicalTreeParser oldTreeIter = new CanonicalTreeParser();
        ObjectId oldTree = git.getRepository().resolve( commitHashPrev+"^{tree}" ); // HEAD~1^{tree}
        oldTreeIter.reset( reader, oldTree );
        CanonicalTreeParser newTreeIter = new CanonicalTreeParser();
        ObjectId newTree = git.getRepository().resolve( commitHashNext+"^{tree}" ); // HEAD^{tree}
        newTreeIter.reset( reader, newTree );

        DiffFormatter diffFormatter = new DiffFormatter( DisabledOutputStream.INSTANCE );
        diffFormatter.setRepository( git.getRepository() );
        List<DiffEntry> entries = diffFormatter.scan( oldTreeIter, newTreeIter );

        for( DiffEntry entry : entries ) {
            if (!"/dev/null".equals(entry.getNewPath())) {
                array.add(entry.getNewPath());
                //System.out.println( entry.getNewPath() );
            }
        }

        return Arrays.copyOf(array.toArray(), array.size(), String[].class);
    }


    public static void main(String[] args) throws IOException, GitAPIException {
        String repoPath = "C:\\Users\\rafal.braun\\Desktop\\gogs\\.git";
        //String[] array = GitApi.getAllBranches(repoPath);
        //String[] array = GitApi.getAllCommitsPerBranch(repoPath, "refs/heads/main");
        //String[] array = GitApi.getAllFilesPerCommit(repoPath, "3c49a61", "72c2d6a");

        //System.out.println(Arrays.toString(array));
    }

}