package explorer;

import org.eclipse.jgit.api.errors.GitAPIException;

import javax.swing.*;
import java.awt.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileSystemView;
import javax.swing.text.BadLocationException;
import javax.swing.text.StyleContext;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class RepoExplorer {
    private static String PROJECT_PATH;
    private static String REPOSITORY_PATH;
    private static String BRANCH_NAME;

    private static final int LIST_WIDTH = 580;
    private static final int TREE_WIDTH = 400;
    private static final int TEXT_WIDTH = 860;
    private static final int HEIGHT = 910;

    private static Document doc = new Document();

    protected static void updateLabel(String name) {
        System.out.println(name);
    }
/*
    public void setTextContent(String content) {
        text.setText(content);
    }

 */
/*
    //TODO make normal function that builds file tree
    private DefaultMutableTreeNode createTreeNodes() {
        DefaultMutableTreeNode top = new DefaultMutableTreeNode(PROJECT_PATH);

        DefaultMutableTreeNode node = new DefaultMutableTreeNode("templates");
        top.add(node);

        DefaultMutableTreeNode node1 = new DefaultMutableTreeNode("templates");
        DefaultMutableTreeNode node2 = new DefaultMutableTreeNode("admin");
        node1.add(node2);
        top.add(node1);

        return top;
    }
*/
    private static DefaultMutableTreeNode createTreeNodes(Path path) {
        FileSystemView fileSystemView = FileSystemView.getFileSystemView();
        DefaultMutableTreeNode top = new DefaultMutableTreeNode(path.toFile());
        DefaultMutableTreeNode node;

        File[] files = fileSystemView.getFiles(path.toFile(), true);
        Arrays.sort(files);
        for (File file : files) {
            if (file.isDirectory()) {
                node = createTreeNodes(file.toPath());
                top.add(node);
            }
            if (file.isFile()) {
                node = new DefaultMutableTreeNode(file);
                top.add(node);
            }
        }
        return top;
    }


    public static void setTreeExpandedState(JTree tree, boolean expanded) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getModel().getRoot();
        setNodeExpandedState(tree, node, expanded);
    }

    public static void setNodeExpandedState(JTree tree, DefaultMutableTreeNode node, boolean expanded) {
        ArrayList<DefaultMutableTreeNode> list = Collections.list(node.children());
        for (DefaultMutableTreeNode treeNode : list) {
            setNodeExpandedState(tree, treeNode, expanded);
        }
        if (!expanded && node.isRoot()) {
            return;
        }
        TreePath path = new TreePath(node.getPath());
        if (expanded) {
            tree.expandPath(path);
        } else {
            tree.collapsePath(path);
        }
    }

    public void createGui() throws IOException, GitAPIException {

        // tree panel
        /*
        JTree tree = new JTree(createTreeNodes(Paths.get(PROJECT_PATH)));
        setTreeExpandedState(tree, true);
        JScrollPane treeScrollPane = new JScrollPane(tree);
*/

        /*******************************************************************8
         * tree panel
         */

        DefaultMutableTreeNode top = createTreeNodes(Paths.get(PROJECT_PATH));
        FileManager fileManager = new FileManager(top);
        JScrollPane treeScrollPane = new JScrollPane(fileManager);
        fileManager.addTreeSelectionListener(new TreeSelectionListener() {
            public void valueChanged(TreeSelectionEvent e) {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.getPath().getLastPathComponent();
                File file = (File) node.getUserObject();
                //System.out.println(file.getPath());

                try {
                    doc.loadFile(file.getPath());
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                } catch (BadLocationException badLocationException) {
                    badLocationException.printStackTrace();
                }
            }
        });

        /*******************************************************************8
         * text panel
         */

        /*
        text = new JTextArea();
        JScrollPane textScrollPane = new JScrollPane(text);
        Font font = new Font(Font.MONOSPACED, Font.PLAIN, 14);
        text.setEditable(false);
        text.setFont(font);
        text.setTabSize(2);
*/

        JTextPane text = new JTextPane();
        JScrollPane textScrollPane = new JScrollPane(text);
        text.setFont(new Font(Font.MONOSPACED, Font.TRUETYPE_FONT, 14));
        text.setDocument(doc);
        textScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

        //doc.loadFile(doc, "/home/rafal/IdeaProjects/RepoExplorer/src/main/java/explorer/FileManager.java");

        /*******************************************************************8
         * list panel
         */

        String[] data = GitApi.getAllCommitsPerBranch(REPOSITORY_PATH, BRANCH_NAME);
        JList<String> list = new JList<String>(data);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        //list.setSelectedIndex(0); -- after we make sure it works
        JScrollPane listScrollPane = new JScrollPane(list);
        list.addListSelectionListener(e -> {
            //System.out.println(list.getSelectedValue());
            String hash1 = list.getSelectedValue();
            String hash2 = list.getModel().getElementAt(list.getSelectedIndex()+1);
            System.out.println(hash1 + " -> " + hash2);
        });
        list.setCellRenderer(
                new DefaultListCellRenderer() {
                    @Override
                    public Component getListCellRendererComponent(JList list, Object value, int index,
                                                                  boolean isSelected, boolean cellHasFocus) {
                        JLabel renderer = (JLabel) super.getListCellRendererComponent(list, value, index,
                                isSelected, cellHasFocus);
                        //renderer.setBorder(cellHasFocus ? focusBorder : noFocusBorder);
                        Font font = new Font(Font.MONOSPACED, Font.PLAIN, 12);
                        renderer.setFont(font);
                        return renderer;
                    }
                }
        );

        String[] branches = GitApi.getAllBranches(REPOSITORY_PATH);
        System.out.println(Arrays.toString(branches));

        JComboBox petList = new JComboBox(branches);
        petList.addActionListener(e -> {
            JComboBox cb = (JComboBox)e.getSource();
            String petName = (String)cb.getSelectedItem();
            updateLabel(petName);
        });

        /*******************************************************************8
         * frame
         */

        //JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame frame = new JFrame("RepoExplorer");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Create and set up a frame window
        JPanel panel = new JPanel();

        JSplitPane rightSplitPanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        rightSplitPanel.setTopComponent(treeScrollPane);
        rightSplitPanel.setBottomComponent(textScrollPane);
        rightSplitPanel.setDividerLocation(600);

        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        splitPane.setTopComponent(listScrollPane);
        splitPane.setBottomComponent(rightSplitPanel);
        splitPane.setDividerLocation(350);

        //Add the split pane to this panel.
        panel.add(splitPane);


        /*
        panel.setLayout(null);
        petList.setBounds(10,10, LIST_WIDTH,20);
        panel.add(petList);
        listScrollPane.setBounds(10,40,LIST_WIDTH,HEIGHT);
        panel.add(listScrollPane);
        treeScrollPane.setBounds(20+LIST_WIDTH,40,TREE_WIDTH,HEIGHT);
        panel.add(treeScrollPane);
        textScrollPane.setBounds(20+LIST_WIDTH+TREE_WIDTH+10,40,TEXT_WIDTH,HEIGHT);
        panel.add(textScrollPane);

        panel.setPreferredSize(new Dimension(20+LIST_WIDTH+TREE_WIDTH+TEXT_WIDTH+20, HEIGHT+50));
        */

        // Set the window to be visible as the default to be false
        frame.add(panel);
        frame.pack();
        frame.setVisible(true);
    }

    private static void init() {
        String os = System.getProperty("os.name");

        //decide if linux or windows
        if (os.toLowerCase().contains("win")) {
            REPOSITORY_PATH = "C:\\Users\\rafal.braun\\Desktop\\gogs\\.git";
            BRANCH_NAME = "refs/heads/main";
            PROJECT_PATH = "C:\\Users\\rafal.braun\\Desktop\\gogs";
        } else {
            REPOSITORY_PATH = "/home/rafal/gogs/.git";
            BRANCH_NAME = "master";
            PROJECT_PATH = "/home/rafal/gogs";
        }
    }

    public static void main(String[] args) throws IOException {
        init();
        try {
            // Significantly improves the look of the output in
            // terms of the file names returned by FileSystemView!
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

            RepoExplorer gui = new RepoExplorer();
            gui.createGui();
            //gui.loadFile("src/explorer/FileManager.java");

        } catch(Exception e) {
            e.printStackTrace();
            System.err.println(e.toString());
        }
    }
}