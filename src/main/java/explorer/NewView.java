package explorer;

import org.eclipse.jgit.api.errors.GitAPIException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileSystemView;
import javax.swing.text.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

/** To enable no wrap to JTextPane **/
class ExtendedStyledEditorKit extends StyledEditorKit {
	private static final long serialVersionUID = 1L;

	private static final ViewFactory styledEditorKitFactory = (new StyledEditorKit()).getViewFactory();

	private static final ViewFactory defaultFactory = new ExtendedStyledViewFactory();

	public Object clone() {
		return new ExtendedStyledEditorKit();
	}

	public ViewFactory getViewFactory() {
		return defaultFactory;
	}

	/* The extended view factory */
	static class ExtendedStyledViewFactory implements ViewFactory {
		public View create(Element elem) {
			String elementName = elem.getName();
			if (elementName != null) {
				if (elementName.equals(AbstractDocument.ParagraphElementName)) {
					return new ExtendedParagraphView(elem);
				}
			}

			// Delegate others to StyledEditorKit
			return styledEditorKitFactory.create(elem);
		}
	}

}

class ExtendedParagraphView extends ParagraphView {
	public ExtendedParagraphView(Element elem) {
		super(elem);
	}

	@Override
	public float getMinimumSpan(int axis) {
		return super.getPreferredSpan(axis);
	}
}

public class NewView {

	//private static String PROJECT_PATH = "C:\\Users\\rafal.braun\\IdeaProjects\\repoexplorer";

	private static String PROJECT_PATH = "C:\\Users\\rafal.braun\\Desktop\\gogs";
	private static String BRANCH_NAME = "main";

	//private static String PROJECT_PATH = "/home/rafal/gogs";
	//private static String BRANCH_NAME = "master";

	//private static String PROJECT_PATH = "C:\\Users\\rafal.braun\\Desktop\\Crosig";
	//private static String BRANCH_NAME = "develop";

	private static FileManager fileManager;
	private static JList<String> fileList = new JList<>();

	private static DefaultMutableTreeNode createTreeNodes(Path path) {
		FileSystemView fileSystemView = FileSystemView.getFileSystemView();
		DefaultMutableTreeNode top = new DefaultMutableTreeNode(path.toFile());
		DefaultMutableTreeNode node;

		File[] files = fileSystemView.getFiles(path.toFile(), true);
		Arrays.sort(files);
		for (File file : files) {
			if (file.isDirectory()) {
				node = createTreeNodes(file.toPath());
				top.add(node);
			}
			if (file.isFile()) {
				node = new DefaultMutableTreeNode(file);
				top.add(node);
			}
		}
		return top;
	}

	public static void main(String[] args) throws IOException, GitAPIException {

		JFrame frame = new JFrame("tmp");
		JPanel panel = new JPanel(new GridLayout(1,0));

		DefaultMutableTreeNode top = createTreeNodes(Paths.get(PROJECT_PATH));
		fileManager = new FileManager(top);
		JScrollPane treeScrollPane = new JScrollPane(fileManager);

		//JTextPane textPane = new NoWrapJTextPane();
		//JScrollPane textScrollPane = new JScrollPane( textPane );

		JTextPane textPane = new JTextPane(new DefaultStyledDocument());
		textPane.setEditorKit(new ExtendedStyledEditorKit());
		JScrollPane textScrollPane = new JScrollPane( textPane );

		JSplitPane tab1SplitPanel = new JSplitPane();
		tab1SplitPanel.setTopComponent(treeScrollPane);
		tab1SplitPanel.setBottomComponent(textScrollPane);
		tab1SplitPanel.setDividerLocation(400);

		// TEXT PANE TAB 2
		JTextPane textPane2 = new JTextPane(new DefaultStyledDocument());
		textPane2.setEditorKit(new ExtendedStyledEditorKit());
		JScrollPane textScrollPane2 = new JScrollPane( textPane2 );

		// TAB 2
		JSplitPane tab2SplitPanel = new JSplitPane();
		JScrollPane fileListScrollPane = new JScrollPane(fileList);
		tab2SplitPanel.setTopComponent(fileListScrollPane);
		tab2SplitPanel.setBottomComponent(textScrollPane2);
		tab2SplitPanel.setDividerLocation(400);

		fileList.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				String fname = fileList.getSelectedValue();
				System.out.println("---> " + fname);
				
				File file = new File(PROJECT_PATH+"/"+fname);
				String content = null;
				try {
					content = new String(Files.readAllBytes(Paths.get(file.getPath())));
				} catch (IOException ioException) {
					ioException.printStackTrace();
				}
				textPane2.setText(content);
			}
		});

		//String[] data = {"one", "two", "three", "four"};
		String[] data = GitApi.getAllCommitsPerBranch(PROJECT_PATH+"/.git", BRANCH_NAME);
		JList<String> list = new JList<>(data);
		JScrollPane listScrollPane = new JScrollPane(list);
		list.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent listSelectionEvent) {

				String hash1 = list.getSelectedValue().substring(0, 8);
				String hash2 = list.getModel().getElementAt(list.getSelectedIndex()+1).substring(0, 8);

				//System.out.println(hash1);

				// SET VALUES FOR TAB 1

				// checkout to new hash
				//git.checkout().setName("<id-to-commit>").call();
				try {
					GitApi.checkoutToCommit(PROJECT_PATH+"/.git", hash1);
				} catch (IOException | GitAPIException e) {
					e.printStackTrace();
					System.err.println("Error: Git checkout not succeeded.");
				}

				DefaultTreeModel model = (DefaultTreeModel)fileManager.getModel();

				DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();
				root.removeAllChildren();

				DefaultMutableTreeNode top = createTreeNodes(Paths.get(PROJECT_PATH));
				model.setRoot(top);
				model.reload(top);

				// SET VALUES FOR TAB 2
				try {
					String[] data = GitApi.getAllFilesPerCommit (PROJECT_PATH+"/.git", hash1, hash2);
					DefaultListModel<String> theNewModel = new DefaultListModel<>();

					for (String s : data) {
						theNewModel.addElement(s);
					}

					fileList.setModel( theNewModel );
				} catch (IOException | GitAPIException e) {
					e.printStackTrace();
				}


			}
		});

		// NOTEBOOK
		JTabbedPane tabbedPane =new JTabbedPane();
		tabbedPane.add("full", tab1SplitPanel);
		tabbedPane.add("diff", tab2SplitPanel);

		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		splitPane.setTopComponent(listScrollPane);
		splitPane.setBottomComponent(tabbedPane);
		splitPane.setDividerLocation(160);

		fileManager.addTreeSelectionListener(new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent e) {
				DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.getPath().getLastPathComponent();
				File file = (File) node.getUserObject();
				//System.out.println(file.getPath());

				if (!file.isDirectory()) {
					try {
						String content = new String(Files.readAllBytes(Paths.get(file.getPath())));
						textPane.setText(content);

						Font font = new Font(Font.MONOSPACED, Font.BOLD, 14);
						textPane.setFont(font);

					} catch (IOException ex) {
						ex.printStackTrace();
						System.err.println("Error: No such file?");
					}
				}
			}
		});

		panel.add(splitPane);
		panel.setPreferredSize(new Dimension(1600, 800));

		frame.add(panel);
		frame.pack();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
