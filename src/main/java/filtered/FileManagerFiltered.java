package filtered;

import javax.swing.*;
import java.awt.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeSelectionModel;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

public class FileManagerFiltered extends JTree {

	private final int rowIconPadding = 6;

	public FileManagerFiltered(DefaultMutableTreeNode top) {
		super(top);

		addTreeSelectionListener(new CustomTreeSelectionListener());
		setCellRenderer(new FileTreeCellRenderer());
		getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
	}

	private static boolean anyStartsWith(String prefix, ArrayList<String> list) {
		for(String s : list) {
			if (s.startsWith(prefix)) {
				return true;
			}
		}
		return false;
	}

	private static DefaultMutableTreeNode createFilesystemNodes(Path path, ArrayList<String> diff) {
		FileSystemView fileSystemView = FileSystemView.getFileSystemView();

		DefaultMutableTreeNode top = new DefaultMutableTreeNode(path.toFile());
		DefaultMutableTreeNode node;

		File[] files = fileSystemView.getFiles(path.toFile(), true);
		Arrays.sort(files);
		for (File file : files) {

			//System.out.println(file.getPath() + ":" + diff + " --- " + diff.contains(file.getPath()) );
			//if (file.getPath().startsWithOneOf(diff)) //TODO

			if (anyStartsWith(file.getAbsolutePath(), diff)) {
			//if (diff.contains(file.getPath())) {

				if (file.isDirectory()) {
					node = createFilesystemNodes(file.toPath(), diff);
					top.add(node);
				}
				if (file.isFile()) {
					node = new DefaultMutableTreeNode(file);
					top.add(node);
				}

			}
		}
		return top;
	}

	public static class CustomTreeSelectionListener implements TreeSelectionListener {
		public void valueChanged(TreeSelectionEvent e) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.getPath().getLastPathComponent();
			File file = (File) node.getUserObject();
			System.out.println(file.getPath());
			//showChildren(node);
		}
	}

	private static void createAndShowGUI() {
		//Create and set up the window.
		JFrame frame = new JFrame("FileMan");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		ArrayList<String> diff = new ArrayList<>();

		//diff.add("C:\\Users\\rafal.braun\\Desktop\\jgit-examples\\src\\main\\java\\explorer\\GitApi.java");
		//diff.add("C:\\Users\\rafal.braun\\Desktop\\jgit-examples\\pom.xml");
		//String topDir = "C:\\Users\\rafal.braun\\Desktop\\jgit-examples";

		diff.add("/home/rafal/IdeaProjects/RepoExplorer/src/main/java/explorer/GitApi.java");
		diff.add("/home/rafal/IdeaProjects/RepoExplorer/pom.xml");
		String topDir = "/home/rafal/IdeaProjects/RepoExplorer";

		DefaultMutableTreeNode top = createFilesystemNodes(Paths.get(topDir), diff);
		FileManagerFiltered fileManagerFiltered = new FileManagerFiltered(top);
		JScrollPane treeView = new JScrollPane(fileManagerFiltered);

		frame.add(treeView);

		//Set size
		frame.setPreferredSize(new Dimension(1000, 1000));

		//Display the window.
		frame.pack();
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}

class FileTreeCellRenderer extends DefaultTreeCellRenderer {
	private final FileSystemView fileSystemView;
	private final JLabel label;

	FileTreeCellRenderer() {
		label = new JLabel();
		label.setOpaque(true);
		fileSystemView = FileSystemView.getFileSystemView();
	}

	@Override
	public Component getTreeCellRendererComponent(
			JTree tree,
			Object value,
			boolean selected,
			boolean expanded,
			boolean leaf,
			int row,
			boolean hasFocus) {

		DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
		File file = (File)node.getUserObject();
		label.setIcon(fileSystemView.getSystemIcon(file));
		label.setText(fileSystemView.getSystemDisplayName(file));
		label.setToolTipText(file.getPath());

		if (selected) {
			label.setBackground(backgroundSelectionColor);
			label.setForeground(textSelectionColor);
		} else {
			label.setBackground(backgroundNonSelectionColor);
			label.setForeground(textNonSelectionColor);
		}

		return label;
	}
}
